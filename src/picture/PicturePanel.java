package picture;
/**
 * PicturePanel.java
 * Author: Chuck Cusack
 * Date: August 22, 2007
 * Version: 2.0
 * 
 * Modified August 22, 2008
 *
 *An almost blank picture.
 *It just draws a few things.
 *
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * A class draws a picture on a panel
 *
 */
public class PicturePanel extends JPanel 
{
    private TableCloth theTableCloth;
    private Pizza thePizza;
    
    ArrayList<Pepperoni> pepperoniList = new ArrayList<Pepperoni>();
    ArrayList<Mushroom> mushroomList = new ArrayList<Mushroom>();
    ArrayList<Ham> hamList = new ArrayList<Ham>();
    
    private int numberOfPepperoni;
    private int numberOfMushrooms;
    private int numberOfHam;
    
    
    /**
     * Get stuff ready so when paintComponent is called, it can draw stuff.
     * Depending on how complicated you want to get, your constructor may be blank.
     */
    public PicturePanel() 
    {
        // If you want to handle mouse events, you will need the following
        // 3 lines of code.  Just leave them as is and modify the methods
        // with "mouse" in the name toward the end of the class.
        // If you don't want to deal with mouse events, delete these lines.
        MouseHandler mh=new MouseHandler();
        addMouseListener(mh);
        addMouseMotionListener(mh);
        
        // Initialize number of mouse clicks to 0.
    }

    /**
     * This method is called whenever the applet needs to be drawn.
     * This is the most important method of this class, since without
     * it, we don't see anything.
     * 
     * This is the method where you will most likely do all of your coding.
     */
    public void paintComponent(Graphics g) 
    {
        // Always place this as the first line in paintComponent.
        super.paintComponent(g);
        
        // Some random things.  Remove and replace with your code.
	    g.setColor(Color.red);
	    g.fillRect(10,10,10,10);  
	    
	    theTableCloth = new TableCloth(0,0,1000,600,new Color(108, 79, 58), new Color(232, 199, 158));
    	theTableCloth.displayTableCloth(g);
    	
    	thePizza = new Pizza(350,150,300,300,new Color(150, 89, 58), new Color(182, 0, 27), new Color (219, 199, 101));
    	thePizza.displayPizza(g);
    	
    	
    	// displays my name and the date in bottom right corner
    	g.setColor(new Color(191, 157, 124));
    	g.fillRect(872, 510, 115, 45);
    	
    	g.setColor(new Color(182, 0, 27));
		g.setFont(new Font("Times New Roman", Font.BOLD, 20));
		g.drawString("Anna Prins", 880, 529);
		g.drawString("Jan 22 2019", 880, 549);
    	
		// displays the instructions in the top left corner of the screen
		g.setColor(new Color(191, 157, 124));
    	g.fillRect(5, 5, 380, 125);
		
    	g.setColor(new Color(182, 0, 27));
		g.setFont(new Font("Times New Roman", Font.BOLD, 25));
		g.drawString("Instructions:", 10, 30);
		g.drawString("Left click to add a pepperoni", 10, 60);
		g.drawString("Right click to add a mushroom", 10, 90);
		g.drawString("Click the mouse wheel to add ham", 10, 120);
		
		// displays the recipe in the top right corner of the screen
		g.setColor(Color.white);
		g.fillRect(700, 20, 250, 145);
		
		// writes a "recipe" for the pizza
		g.setColor(Color.black);
		g.setFont(new Font ("Times New Roman", Font.BOLD, 25));
		g.drawString("Your Pizza Recipe:", 710, 50);
		g.setFont(new Font ("Times New Roman", Font.BOLD, 20));
		g.drawString("Pepperoni: " + numberOfPepperoni, 710, 100);
		g.drawString("Mushrooms: " + numberOfMushrooms, 710, 125);
		g.drawString("Ham: " + numberOfHam, 710, 150);
		
		for(Pepperoni p: pepperoniList) {
			p.drawPepperoni(g);
		}
		
		for(Mushroom m: mushroomList) {
			m.drawMushroom(g);
		}
		
		for(Ham h: hamList) {
			h.drawHam(g);
		}
		
    }
    
    //------------------------------------------------------------------------------------------
    
     //---------------------------------------------------------------
    // A class to handle the mouse events for the applet.
    // This is one of several ways of handling mouse events.
    // If you do not want/need to handle mouse events, delete the following code.
    //
    private class MouseHandler extends MouseAdapter implements MouseMotionListener
    {
      
        public void mouseClicked(MouseEvent e) 
        {
        	
        	// draws a pepperoni when the mouse is left clicked
        	if(e.getButton()==MouseEvent.BUTTON1) {
	            Pepperoni p = new Pepperoni(e.getX(), e.getY(), 20, 20, Color.red);
	            pepperoniList.add(p);
	        	++numberOfPepperoni;
	        	
        	}
        	
        	if(e.getButton()==MouseEvent.BUTTON2) {
        		Ham h = new Ham(e.getX(), e.getY());
        		hamList.add(h);
        		++numberOfHam;
        		
        	}
        	
        	// draws a mushroom when the mouse is right clicked
        	if(e.getButton()==MouseEvent.BUTTON3) {
        		Mushroom m = new Mushroom(e.getX(), e.getY());
        		mushroomList.add(m);      		
        		++numberOfMushrooms;
        		
        	}
        	
            // After making any changes that will affect the way the screen is drawn,
            // you have to call repaint.
            repaint();
        }

        public void mouseEntered(MouseEvent e)
        {
        }
        public void mouseExited(MouseEvent e) 
        {
        }        
        public void mousePressed(MouseEvent e) 
        {
            
        }        
        public void mouseReleased(MouseEvent e) 
        {
        }
        public void mouseMoved(MouseEvent e) 
        {
        }
        public void mouseDragged(MouseEvent e) 
        {
        }
    }
    // End of MouseHandler class
    
} // Keep this line--it is the end of the PicturePanel class